import sys
import csv
import os.path

def get_cleaned_data(path):
  reader = csv.DictReader(path)
  return reader

def transform_data(headers, data):
  cleaned_headers = []
  cleaned_data = []

  for header in headers:
    cleaned_headers.append(header)

  for data_item in data:
    cleaned_data.append(data_item)

  return cleaned_headers, cleaned_data

def write_data(outfile, header, data):
  writer = csv.DictWriter(outfile, header)
  writer.writeheader()
  writer.writerows(data)


if __name__ == "__main__":
  try:
    input_file = open(sys.argv[1], 'r')
    cleaned_data = get_cleaned_data(input_file)

    output_file = open(sys.argv[2], 'w+')
    transformed_header, transformed_data = transform_data(cleaned_data.fieldnames, cleaned_data)

    write_data(output_file, transformed_header, transformed_data)
  except (IndexError, IOError) as err:
    print "%s" % (err,)
